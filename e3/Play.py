#!/usr/bin/env python

import json
import logging
import logging.config
import os
import sys
import time
from threading import Thread

import click
import requests
import spur
from spur.results import ExecutionResult

from common import Utils
from common.Config import run_config
from common.E3 import e3
from common.Utils import LogWrapper
from provisioning.Aws import Aws


class Play:
    """
    This class runs an run as defined in an run file.
    """

    def __init__(self, run_name):
        self._log = logging.getLogger('run')
        self._run_config = e3.load_run(run_name)
        self._run_name = run_name
        self.denormalize_config()

    def denormalize_config(self):
        """
        This method goes through the run configuration and de-normalises all the stage configuration into
        independent stages. This means that each stage should have all the information it needs to run without having to
        rely on any information from the parent config. If a stage does not a required attribute then that property is
        copied from the top level run configuration.
        :return: an run configuration dict comprising fully denormalized stages
        """
        self.denormalize_attribute("duration")
        self.denormalize_attribute("workload")

    def denormalize_attribute(self, attribute_name):
        """
        Denormalizes the supplied attribute in the run configuration
        :param attribute_name: the name of the attribute to denormalize
        :return: a set of attribute values
        """
        threads = self._run_config["threads"]
        attributes = set()
        if attribute_name in self._run_config:
            attribute = self._run_config[attribute_name]
            attributes.add(attribute)
            self._log.debug("Found a top level attribute %s (%s) in configuration, applying to all %s-less stages",
                            attribute_name, attribute, attribute_name)
            for thread in threads:
                for stage in thread["stages"]:
                    if attribute_name not in stage:
                        stage[attribute_name] = attribute
                    attributes.add(stage[attribute_name])
        else:
            self._log.debug("No top level [%s] attribute found, checking that each stage contains one", attribute_name)
            for thread in threads:
                for stage in thread["stages"]:
                    if attribute_name not in stage:
                        raise Exception("Stage [%s] does not have attribute [%s] and no top level instance defined" %
                                        (stage, attribute_name))
        return attributes

    def run(self):
        run_threads = []
        thread_index = 1
        for thread in self._run_config["threads"]:
            instance = thread["instance"];
            stack = instance["stack"];
            i = stack["StackId"]
            return i

        #    self._log.info("Stack %s" % i)
        #    thread_index += 1
        #    run_threads.append(self._run_name)
        #self._log.info("Finished reading %s" % self._run_name)
        #return self._run_name



@click.command()
@click.option('-r', '--run', required=True, help='The experiment run you want to execute',
              type=click.Choice(e3.get_runs()), default=e3.get_single_run())
def command(run):
    e3.setup_logging()
    aws = Aws()
    run_inst = Play(run)
    i = run_inst.run()

    #fs = aws.cloud_formation.StackResource(i, 'FileServer')
    #print fs 
    #print '\n'

    cf = aws._session.client("cloudformation")
    #print '\nDESCRIBE\n'
    d = cf.describe_stack_resource(StackName=i, LogicalResourceId='FileServer')
    #print d

    inst = d['StackResourceDetail']['PhysicalResourceId']
    print inst
    rezs = aws.ec2.describe_instances(InstanceIds=[inst])
    ip = rezs['Reservations'][0]['Instances'][0]['PublicIpAddress']
    print ip 
    print '\n'

if __name__ == '__main__':
    command()
